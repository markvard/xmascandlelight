/*
 * ----------------------------------------------------------------------------
 * "THE SODA-WARE LICENSE" (Revision 42):
 * <renemarkvard@gmail.com> wrote this file.  As long as you retain this notice 
 * you can do whatever you want with this stuff. If we meet some day, and you 
 * think this stuff is worth it, you can buy me a soda in return.   
 * René Markvard (Yep, this is a rewrite of the BEER-ware licene by PHK)
 * ----------------------------------------------------------------------------
 */
(function($) {

	$.fn.xmascandlelight = function (options) {
		
		var that = this;
		
		var defaults = {
			container: that,
			stand: $("<img/>").attr({
				width: "150",
				height: "69",
				src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAABFCAYAAACoqx9jAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OUU4MUZBQzFBNEVCMTFFNjgyOEFENzNBRUUzMThENkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OUU4MUZBQzJBNEVCMTFFNjgyOEFENzNBRUUzMThENkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5RTgxRkFCRkE0RUIxMUU2ODI4QUQ3M0FFRTMxOEQ2QiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5RTgxRkFDMEE0RUIxMUU2ODI4QUQ3M0FFRTMxOEQ2QiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PndYHhcAAAJxSURBVHja7J2xS1VhGIfPkYtJIeQSJE7V0OAUhBbWfyCEQ05yg6JNoiUQGgRpaXMRcROUKAdvNCRIIEE2KFTS1qBILiEuV7ik4PX9uHcRJFr0vr/3PA/8cP2d8z2c830fx+/m9Xo988CHPE9/fJTRJx9s8bi2MQaAWIBYgFgAiAWIBYgFgFggQMlZn5whiUHuZecdeBUCIBYgFiAWgNaq8L9pfmYTnkHRxZX6E+uZ5ShrfMcVKYeWpzyxWsekpWaZzuLsgR1Yhi0V5litZcYyEejt91hdqkiT93HLpwDXkZ7Ac6wK/ZDmJWXLnvA1bFhesN3gjx3LqGj3tAB50pxfIZZD3li+CPaetaxFGohoYqVX4kvBVeBEsHEIufO+YlkV6vvOsolYOqsrFaYiDkBUsSoiK8Rflq+IpTVvWRTo+Tbo/Q/9dcN7gY4fEUtzEn/ouF81C7bFUBSx0sB9d9xv3bn4iPUPvjnuthH5xkcX64fjbj8RS5ctx922EUuX33RDrLPgj+Nuu4ilS81xt7+Ipcu+425VxOL6gBt/gkuOu3Uili5XHXe7gFi6XHfcrRuxdLnluFsPYulyz3G3m4ilyWXLfcf9biOWJg8t7Y77DWSBz1yNKlYaMO//vNrtfA6IWKcwZOkV6FlGLB06LK9Fuo5YuhBLg1eWa0ILjDHE8s8Dy3OxzqlvP2L5pc8yL7jSSqcqLlhuIJY/7lqWLBdF+6dd+HRKzh3E8sMjy3JzvqLMFcvnrHGWQ6/6oMj+lg7HcSMWFBC+sATEAsSCglMqyiQYzlmsrHEgLACvQkAsQCwAxALEAsQCQCxALCgcxwIMAOxykVzKLNseAAAAAElFTkSuQmCC"
			}),
			flame: $("<img/>").attr({
				width: "50",
				height: "57",
				src: "data:image/gif;base64,R0lGODlhMgA5AOZ/ANmBgbsdHbgVFf35+cI0NMdGRroZGeetrdNsbMlNTfvz8/nr6+Ohofrv7+7IyL8qKshJSdh+frQFBcxWVvDOzt+VlfTc3Pz19e3ExMExMfXe3ui0tPfk5PLV1eq4uPDMzPjp6bYODtBlZbcQEPHQ0Oq6utyMjO3Cwum2ttd6euSmptuIiOewsNRxcfPW1sAuLu7GxsM4OPbh4bMCAvTa2spQUPbg4M5dXbwiIuiyst2Ojuu9vbUKCtRwcOCYmNyKis5cXOSkpNV0dOGdndqEhOKfn+zAwMQ8PLQHB81ZWdJqas1aWtqGhtZ3d+WpqbYMDMZCQr0kJN6SkstSUtBiYsU+PrsfH+Gams9gYMQ6OtZ2du/JybIBAf79/frw8Pz29v78/P36+rMEBP/+/vvx8bgTE/ns7P339+WqqvTZ2f77+9d8fMVAQN2QkPfm5vPY2Ou+vt6Rkb4nJ/rt7ey/v/jn59JpactUVOarq/HS0tFoaL0mJtNubrIAAP///////yH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0ZDI1ZTdkMi0xYjgxLTQ0NGMtOTA0MS1kNzY0NzgzZWYzNmEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RkNFMERCM0NBNEVBMTFFNkI0OTQ4QkI0RDE4NjdCOTEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RkNFMERCM0JBNEVBMTFFNkI0OTQ4QkI0RDE4NjdCOTEiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0ZDI1ZTdkMi0xYjgxLTQ0NGMtOTA0MS1kNzY0NzgzZWYzNmEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NGQyNWU3ZDItMWI4MS00NDRjLTkwNDEtZDc2NDc4M2VmMzZhIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkECQAAfwAsAAAAADIAOQAAB/+Af4KDhIWGgiZdh4uMjY6Cbkgqj5SVi2t9L5abll8CfX0HnKONQ6B9MaSqhgSnfQ6rsVugS099E7GrWKAfdn1iMrmjZDx9GX55oGvCnD6gUn5+WX0GasyWMX0SC9FXoKLXjzSgCdF+CsUF4Y8RoCrmfjd9MxrrjVF9PArwKKAR9osogKoBz0+YT1EAHmoCKkhBPyJAYVBYCB8SMg/hgOJDcVAHUAUe+gHzyYCijkRA6RDphwqoEx3/HAHlgiULUEI6KpDQBwfLc0j6JKTYr4+dn34K0KTYAhQapD9UUnwBigPSDyAVLuDSxwpSP11G5BsA0AkoIF/9QJAIUAgoH2n/U/ZZAZANKAppdwy018XWjAtp54AKYM8CKK9p/VgB1WDdzT4QEvtJwDZc1D5CJDft42OdElBXHqYw87ACznU1QHkoSIFLk4cbQOEK16oPiYKpR5wpiLXPkXUBQLmB50YMKBYFOYDCsU5sHy/wTIPqUdALqDLrgvYZAG/CKSgFw4B6su5UwQenAjw8tW4GKOjmypx6UlABKB7NQYGA5948vAXXrWMAKLeZw9N9BSUj1Dq1IWfOJ6AIUNBjbKyTBCgrwEMVKDEUtAIoVKyDSR/lmLPEKUsUtFYfcaxT1AhhmCNdHyuZM0AId62zEygomJPGKRjAc0CEJ4VjVx8hmYNedAhqwDOTUQCZwqM5H6ZozpAEAuQJKFHAtwAPOZjDAYQQUBTHKQXs5ocJ3PkBAnradECRGrWhogE8RgQHChExWQBhPgikQUcNXMkWkyAYOOeKK1OQdegfJOCwKCgzRADGo4N4gYBxp8SwBaaGWICAAXeUIEwgACH5BAkAAH8ALAAAAAAyADkAAAf/gH+Cg4SFhg0+hoqLjI2LOgIKjpOUjnt9AJWam387fX0jc5yjjUCffRGkqoVkPKcjZKuyV58PbH0rsqtVnyssfQJfuqMcM30zHF1WfVLDnDqfUH5+P30Pzpsxn1fTCxJ9HtiUMlx9EgvTfkl9BeKT0OzpfkZ9XB3ujbfM8mNRfXr4Fs0x1keGPD9EQAkLWEjFpwwH/Wgop4JhoRufIkT0c6QPFIuEBHw6sbHCMQ0g/5D49CTMxjrGMoFs8wnCxmm8cKSs8UnHTT80+zgAWeYTiZ8WPgmx+OYTDzA//TzoY8VikU9VovoR8ikPQyVKtXroxRDKJzxazyDpc4ShgU8G/7V2nBHLXYNPI7ROS/EJBT4Mn7Lo9bPhUwp8Qz5RGVznU1t3XPsQGezn7VN3Ez6pOPilx0YIn9640wfn4AqhEXt8cuJOzicX8rq8nRDRR0Z3b/ugSze2DxIyBwv3ueFuxCcF8vicYnGQAlZ33/oc1PZJy0E3n6K4O3VQZM2DXj6Vgf5JjTwxp+QcBOPUXdE+XuSd6lPm4JhT7vz10SBv7ScJ4H3ChTsd9UGHPDicIsZBHODlzhKfFCFPAq8cBFgfAbiTUB9KyLPGKQEcxMAnBLiDwicvyAPHKQQcBFYfQNhlzAxx+TFACJ/ccdBUfeiAT4GTpYPFJ03I48IpH+BjEn1VA6RDTx+bpfNihvg08ERP8mSwXzokRHdYQC041UE6KoQ4zQDUIcEBQwsYR5UN03QRxzRhZKYUSCN+YsAO8mhQYB9RSAKSHqdwMcEWLvTgCl4kpPSHGnbON98IRjgqCBhNlCNpH3t4ZekgO/DoVAoXfFpIFypUEUUEIOgSCAAh+QQJAAB/ACwAAAAAMgA5AAAH/4B/goOEhYUrhomKi4yKaVw7jZKTjU19BJSZmn9qBn19KpuijCiffXJgo6qFN6Z9QauxFyF9XHqnqbGjaJ9HDU+guqNJn1J+LX0PXcKaA7RcHH4yYn0szJkbnzF+3DV9MdeUVJ8r3H5wnw7hkgKfHeZjD30T64wknwHm3HF9YjL1ilZ8EqHPjxkkfVIATATlE4uCfib0ETBgISEFCGc0gOjhUyiLgkr1yQDRT5cAfaCAFGSpT4+SfizN0LAyy6cDMPN8AgDyjIRaC2D6kRcFpINPUYT6WfPpg0UpnyYovddHiMVWfcopRWnFYoZPJZT6sfOJBEAwCPuYEZutD896b/8+CRDrR8FPcPVyaKPrx6bGevz6LOGb4lOOenw+reGrt0+LelM+FeG74BOmdUc+7ShIYkhJHH2QhFm3p2zBBAYGQExQdl2ZT9HMWeDyCmLLIutGfPKiT2CfAhCLfFIYbsanC/q89ZFApuCOT0vWmSqI8hOKgmm0rUvb3BwPUwAKNvjUNZxutfpc3Sl4Ru666hb0nb8E8ROSdV/7GNEXxVQ+fWOYsg5rfTCgD4F9jFCQF588sQ4yCekTgSkSFKTBJwasI9xv+jw3nT4dJbNOB5/woBo3YbzWBw8FtfFJAvV40gce+owzUUHKEReOjTXoc1Qy+pDxXR8b1INOHzOQoE92ZsCZA9WKFwAkTx8Q6GNEHwRxc0F1SSzEgCk66FOACeYgYIo6AHWR3ww4cdMBHNzoYAqMFuXxUz8/dGHOBUqYEgJNIG34SQZocMBEdbXgsZIgOtDmiiszXLHoIE4A8+hERU46iAYQQIrFHJoagkESBggBaCyBAAAh+QQJAAB/ACwAAAAAMgA5AAAH/4B/goOEhYYNPoaKi4yNizoCCo6TlI57fQCVmpt/O319I3Oco41An30RpKqFZDynI2SrslefD2x9K7KrVZ8rLH0CX7qjHDN9MxxdVn1Sw5w6n1B+fj99D86bMZ9X0wsSfR7YlDJcfRIL035JfQXik9Ds6X5GfVwd7o23zPJjUX16+BbNMdZHhjw/REAJC1hIxacMB/1oKKeCYaEbnyJE9HOkDxSLhAR8OrGxwjENIP+Q+PQkzMY6xjKBbPMJwsZpvHCkrPFJx00/NPs4AFnmE4mfFj4JsfjmEw8wP/086GPFYpFPVaL6EfIpD0MlSrV66MUQyic8Ws8g6XOEoYFPBv+1dpwRy12DTyO0TkvxCQU+DJ+y6PWz4VMKfEM+URlc51Nbd1z7EBns5+1TdxM+qTj4pcdGCJ/euNMH5+AKoRF7fHLiTs4nF/K6vJ0Q0UdGd2/7oEs3tg8SMgcL97nhbsQnBfL4nGJxkAJWd9/6HNT2SctBN5+iuDt1UGTNg14+lYH+SY08MafkHATj1F3RPl7knepT5uCYU+789dEgb+0nCeB9woU7HfVBhzw4nCLGQRzg5c4SnxQhTwKvHARYHwG4k1AfSsizxikBHMTAJwS4g8InL8gDxykEHARWH0DYZcwMcfkxQAif3HHQVH3ogE+Bk6WDxSdNyOPCKR/gYxJ9VQOkQ08fm6XzYob4NPBET/JksF86JER3WEAtONVBOiqEOM0A1CHBAUMLGEeVDdN0Ecc0YWSmFEgjfmLADvJoUGAfUUgCkh6ncDHBFi704ApeJKT0hxp2zjffCEY4KggYTZQjaR97eGXpIDvw6FQKF3xaSBcqVBFFBCDoEggAIfkEBQAAfwAsAAAAADIAOQAAB/+Af4KDhIWGfwdph4uMjY6DMUuPk5SLFH0zLpWblVh9fUmcoo1ePJ8zFqOqhRWfn3qrsX8Zrn08ILKjl583IX1ruaI9riQIfQJfwZVgBp8xfmlcfVfKlCWuV35+UH0v1ZOetmTaKp8Y341fvqDafmdlfUDojCyuKO1+xkhz84c3nyPC4IPxqUK/QmFGfKKCz88YHH0IHCQEx5WHhn5SfOowUZCQT2UENuzwKUXHPw94YdSWEkdHDq7QrMz4acvEIp8keJnpoOREIJ+OzPTTRUCfBxOtfCIy1E+4N/1gmmtark+cfk4+8RA5E4S0BP1afCrQlKWtAfO49YlQ1o8Ic/P/FPa5VxZnnxXobLji0NbFpxrorvUJ0dYPGFMC0LXq86xwjE8cvontg6Ww008evt35ZAJjCjMYV3zS8e3IpwMNKXBpghHNpx7fonxy0LBGnxFnGmL4NOWb0T4a8LkR84lFQwufJFaTu7Pd4j49Gnr5ZOWbtD4X8E1wBaXhl0+Jq8341DDlpwAYPz35Jjd7O3jqG575xOVbsz6g24131bABwG9yfOICPhK4wkNDb3xiwDcFfEKXNr8d0xAKn2TwjRKftIHPC6401k4bn4RSjQ+fTIDPEq4sUdsnAHxDAnhdOOeKDviAAd8G6NxHRztpuIIBPhTaksw3dvyFT0ohqIFPdIN9QDDPFq7A0Y5oKrYTZB8s9FPFJ3I0twAPObQDwn0PdNEPDNdBMYA2JqzpxwK0fILGREV+kkVw7ZwQgCu9TXQBAQYikAYdNVzXjRcn1WFeLbU8ENlJf8zBJKN9JNAApIN00YZc56mAqSFkrBBFDEOgFUsgACH5BAkAAH8ALA4AAAAUADkAAAcygH+Cg4SFhoeIiYqLjI2Oj5CRkpOUlZaXmJmam5ydnp+goaKjpKWmp6ipqqusra6vjIEAIfkECQAAfwAsAAAAADIAOQAAB/+Af4KDhIWGgiZdh4uMjY6Cbkgqj5SVi2t9L5abll8CfX0HnKONQ6B9MaSqhgSnfQ6rsVuufROxq1i0YjK3o2Q8oDcjoGu9nD6nHUKgBmrGljGgVX4aM6Ciz480p0F+fgmgBdmPEaAhF945oDMa441RoHrefmoBoBHuixSnGPN+KaCi5DvUBFSAMf5c8BtYCF4fIf68terDh+GgDqc+RPTThpkii0RA4djoB4S1Pics/jkCig9JPywfWlQgAVSJlzoCWkQBakSYlxpOuWDYAtSEl95egNLBUGmfK0j/hRu4gAsoGlGNgOIxIJ8TZlH9DADWB0O+ZbXC+ikAakU+NqD/KqgFAKqGuy5PQFFQu8OgOwvrvqhtYLVPg3EsAqr15tBsth91F/upAcrHOCWgUpBEc9NfQZnZKPfpFvFLgBgRixgdNxHORimgSPgrAerIOHt93myUA4qIP4x9cLgDZSbih1MJ/JkBVWYcElBkIoZUPE8BqCfjTm0E1zPiqXEnz0Tcc4qLvzBbxw3r4yVimVM8/HlhPs4AKAsRT/ax4i9oH4HZTLSDd6dMM09ffbAxThKVRbReHwj4g0wfVIyDSR83RMQbKCr4swQocYzDUx8BRAQBKFzUMc8Y9vVBwTg0geKAPxcS4I9WfQjwUTZw9QGEPyOa4A93duRjSh9czOjNbgU8SADCPEYURkI+noDyAjreTJDEPAtYAQoEDMVxChQDeLOBbH7MMZEEHTCkxkR9HCHDbLj1QYRKFnxyHQI00DFFYbWoJAgGD9ICyhRdCfoHCTgY2scMEYCh6CBeICCGKzFsMakhFiBgwB0l9BIIACH5BAkAAH8ALAAAAAAyADkAAAf/gH+Cg4SFhoNdP4eLjI2Og2gSbo+UlYsZfUKWm5YefX0hDZyjjQWffVKkqoVpXKdRq7F/CKefHrKkDU+1fRC4oxWntH0zHL+bmH1RYHufRMeVFKdSfjqfsNCPdp9PCn4LEp9G2Y0XI58ifuo1nzfkjCqnH+p+OZ8hZ++HCZ8Z9H5hynzCo69QAySf4vzzI+LTlIKEinySAGJhiU88vkAUBOFTgoV+1Aj4xGKjmXB90IBk+AnLxiEYL6zc8ElAF4js+tRY6ecLj08fCg7Y1YcBTz8d+zzT56mPhAZH23xiU1DIpwJH/eTBOEDfi08mso4Z2WccuQYzPlHI6ifninc0//uU6cJ2hcN3ERyy9XPiU4B3pvpU2KsgbR8z5AT2Wbs3yicM2Th8QqJmb9tPV7Jd7BPDsp8mnzRBs9YHy0oOOhZe+ZQkW49PK1beGOGNXtMq2aZ8OgCShpg+Kv6RuJYtxqctIFu0++emZjYcnzgs7EL2xT8vn0ZkM/CpNj0Mp0b8G/CJS7ZzfUCSJvYvzMRsCMWAvBH+evbzn0B+vfZPht9sAXwi0z/cfQLFPzB88kI2cnxiwUI/hfZPEB5lE1gJC9WCxz8pfNJENsPE9k+EEnjxzxGfoJENTH1gRaCB/yyQFhd1ZJPGZGb8Q8AnRtEjVR8EvONYH6nRs0QfAgzox3IZ0PWhCDl59WGAdyb0AQCHGC3wjgwopaPOFk/MQY8DKIn2jhKnhOVHF4N9qZgBXhRkRoF98PEFPV34ECEXKGzkgWF9WKGDG0EkA9tGggwBKC+fAIDoIAeEwCgoKj46iAZJfTJDEsZYWggGExigRAeyBAIAIfkECQAAfwAsAAAAADIAOQAAB/+Af4KDhIWGhG8Hh4uMjY6DNzGPk5SLFmJ9DpWblVR9fUmcoo1uEp8SIKOqhSmfnyursQojrn04XbGqUrWfHrmjUZ9Inn1Lv5wlriIaXH08CseVU58zNH5sn0XRk26YfTV+fj6fUNuPAK504QszfTMc5oxdAZ8P4eFQn3HxizuuPvfEfTrC7xAxHl4Cgmg3I1XBQQNo9bkREN+nIQ8HHXB1oqKfCp9qZBR059OeMR41fHoyICMZJJ8AeAyH45ORjCpcpZnpR8+nFBknfMrA08/GPpIKRnxVdE4zCRcKJvukoagfOZ/oFGwy1KofLJ+IFMzwKYLXcX2m8GP3yYFXEp//rPDL8WkEGK9qTPVpEI9rnwRew2HtgyFelU86AvtJkM1cFx5tFQv5ae7NqTOK0QIxd5TAzDFrKk4tt43JJ4oe8fShEBBunwfmRDD1SDZFwDqfDJgr8AmPxxOfqgT0Utfcg8gVZfcREDDMKXMGPnGo2EWAsIBjXJmTmDAgBVcShgsz165Pl4polyv8JMCcK498XHm+1+FTlO2fFFTM90lPQBTkQPdJHRXVlE1AOnwigjkEfIJBRRK9E1ASn7RhjlB9FFGRNzFgF10fW5iDTh9YVKRXBQFhUFcY5niQ23n30PKEfvfc8Ikx5nwBWR8sBGRFH0IEpIFevsSD4REohRNDaQgLBMRYH3LgEo8/nwAUThKh3ROHKyo8dMQpOYSjAo1+FFFeFRl94A0SOiTphwIINNPHCBqMZEItMeAhwwrWnbLDSIIgwAsvPKAA6CAAeMNLFCQcSsgHx7nyRARfOFpIFyxAEQURZuQSCAAh+QQJAAB/ACwAAAAAMgA5AAAH/4B/goOEhYaDXT+Hi4yNjoNoEm6PlJWLGX1ClpuWHn19IQ2co40Fn31SpKqFaVynUauxfwinnx6ypA1Pn0qfELijFZ84ajh9MxzAm5h9Jn4rn0TKlRSfSHN+HDN9sNOPdp9LfuOmfUbejRcjn3DjfkGfN+iMKp9RY+4XPKBn84cJnyK4GzflEx5/hRog+WRhoB8nn6YgJFTkUwyHfhTs4/FloiAIn6Rg9FPjEwuPZiQc4zDSxycsHodYHOlHxicBXSaW7LOCpp8Xnz4gHLCrzxufQqIh9NRHjk8/JT6xQZi0T4unF1TyGOAPaJ8cT/3E+HQOXYNtMxqEbfFpxbwNn/8IhH0YcV6ET0LmavgUYF65DXPHhPhkBl2ZT3Xm+snyCYM3Dp8MKPZD5dMVb1H7QJjMBK83HZ+uKobYJ4m3Hp98jHRSwqGRT1W8FezjAeOXABcHvrHnbWyfPBhB93ExcMFNb8b6LHA4JsonAAO9fBrhzcCnLw5hnNrsbswnLt7W9cF495OVgV8+SfC2UALGcqAGNpge/hPG5Pbd7e3Td1qAT2oNNNh0A9HxyQveyPHJUQPV8sBAFfWRgDd/OaTSJzUMxFYfTXhDSx8CDSTAKUQMxAwa3sjURxUO+dbHFu5o0wcXdXiThnrLuaNHZGC4Q0Rc8zjXxw8DMfBJD+4oYN1tkHbd5IU7IMzARQfubMjDAvPIcCEWA0HAnR87iOGZP71E4w4LH4zjgHgGeIGQGUv2gQB243RRwUIzouCRB9uYp4MbQXjVlkeCDNFnLbUAQOggBwyIaAgoLjqIBiCdMkMSyUhaCAYTGKBEB7IEAgAh+QQFAAB/ACwAAAAAMgA5AAAH/4B/goOEhYaCJl2Hi4yNjoJuSCqPlJWLa30vlpuWXwJ9fQeco41DoH0xpKqGBKd9DquxW659E7GrWKA3IX1iMrejZDygJAiga8CcPqAxfmlcfQZqyZYxoFd+flCgotSPNKA8ZNkqoAXejxGgSdl+Z2V9MxrojVGgKO1+xn0R9IsUoEaEyQcDVBR/h5qAopLPzxgcoDAgLGSvj4eGflKA4jNxUAdQZQY2/BhNUUciujBmewDqRMc/R0ChUZkRlJCOCiT0keCFpgODHVGAOkLTT5dPfVxMbAGKSFE/ufromPgi4tNyfc75WwCNh0iaILoO8OfE3NOVEf0JARXhrB8RoP9W+GNzz20RUDXodXkCioNbF6AC0LMAKoRbP2CG9WmAjgWzw36s9ZHo7QcoLJCj+kCnBJQJyCtsoqvBTaWTEg3RgLLlrdUrjF8CNMuHYSi6AKA0YNQBykU+wn1woBsBqme+MRUB5PMCEh0SUBcaFgQFId8XUE/QncKoDpSVhqfQzYDesMApw+3OhBsOykxDiNvbNWjuzUDvhrwC5nsDtHXdfK48kI9QfbCBThKgtNGQTnjl08ZC6GBSS0NI9eFUO6T1EQc6BArQRT6S9bFFO2DA0wcF6OQECh356AGKAWC006FJ3tDVRw35MABKD/mY14cd/pgCChztgDADFx3IeAplCf54AoocxvkBQXXZgGBfHxBMFMcpUAyQDQsfZLNABqBI0MFEarjWRxa6tXMCbk29ZEGFPCCQBh01QLPaS4JgQBwtrkwxFp9/kAAfLTNEAAahg3iBgBiuxLAFo4ZYgIABd5QATCAAOw=="
			}),
			wick:  $("<img/>").attr({
				width: "50",
				height: "57",
				src: "data:image/gif;base64,R0lGODlhMgA5ANUzALIAALcPD7QHB9FmZuWoqPjo6LYNDdh9fcM5Odh/f9qGhui0tNqEhNZ3d/rv79Rycv34+Oq5ufTa2vfm5vbh4c1YWMAtLeKgoLQGBrcQEPLU1LocHLseHslNTc5cXMM3N+m2tvrw8PDNzfvz89Nubv78/MpOTv3398ZDQ9d7e92OjrIBAf36+rgVFd+WlstSUuu+vspQULMCAv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxMzIgNzkuMTU5Mjg0LCAyMDE2LzA0LzE5LTEzOjEzOjQwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOjRkMjVlN2QyLTFiODEtNDQ0Yy05MDQxLWQ3NjQ3ODNlZjM2YSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxNjNBOTE1REE2NTUxMUU2OTEzNEQxNDhCQjk0NjkxNiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxNjNBOTE1Q0E2NTUxMUU2OTEzNEQxNDhCQjk0NjkxNiIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNS41IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjdiMTk0ZGYyLTMyMDctNzY0Yi1hNDJkLTY2MzlkNDE4OWY2ZiIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOmY1NGQwMGEwLWE2NTQtMTFlNi1hZTE1LWJjNzUzODdiMDEzZSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAEAADMALAAAAAAyADkAAAaZwJlwSCwaj8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum8/otHrNbrvf8Lh8Tq/b73gtYUNxhzIAB24qAAAvbh+FHW0OMoUebQuFACRtDJMKbRWTF20IkxFtHJMibQGTE20rkyNtkwBuGIUCbi2FAW4Wt24xs24JkyxtMJMFbRAGhRpuA4UgbhICAC5vKQAPbyUmKHAnDVlBADs="
			}),
			css: {
				candlelightcontainer: {
					fontFamily: 'Arial',
					fontSize: '12px',
					color: '#B20000'
				},
				candle: {
					backgroundImage: 'linear-gradient( to right,  #ffffff, #ccc)',
					backgroundColor: '#ffffff',
					lineHeight: '16px',
					width: '46px',
					textAlign: 'center',
					margin: '0 auto'
				},
				candletop: {
					lineHeight: '8px'
				},
				candlebottom: {
					lineHeight: '20px'
				},
				flame: {
					margin: '0 auto',
					width: '50px'
				},
				stand: {
					margin: '0 auto',
					width: '150px'
				}
			},
			startMonth: 11,
			startDay: 10,
			endMonth: 1,
			endDay: 2,
			candleLightMonth: 12,
			candleWidth: "46px",
			candleCellHeight: 16,
			now: function() {
				return new Date();
			}
		};

		var tmpSettings = $.extend(true, {}, defaults, options);

		var calculatedCss = {
			css: {
				candleDayCell: {position: "relative", height: tmpSettings.candleCellHeight+"px", overflow: "hidden"},
				candleDay: {position: "absolute", bottom: "0px", width: tmpSettings.candleWidth, margin: "0px auto"},
				candle: {width: tmpSettings.candleWidth}
			},
			candleStep: tmpSettings.candleCellHeight / 24
		};
		
		var settings = $.extend(true, tmpSettings, calculatedCss);
		
		if (settings.startDate === undefined) {
			settings.startDate = new Date(settings.now().getFullYear(), settings.startMonth-1, settings.startDay, 0, 0, 0,0);
		}

		if (settings.endDate === undefined) {
			var yearOffset = settings.endMonth < settings.startMonth ? 1 : 0;
			settings.endDate = new Date(settings.now().getFullYear()+yearOffset, settings.endMonth-1, settings.endDay, 23, 59, 0, 0);
		}

		$.data(that, "candleSettings", settings);

		if (isTimeToShowCandle(settings)) {
			var wrapper = $("<div/>").attr({class: "candlelightcontainer"}).css(settings.css.candlelightcontainer).appendTo(settings.container);
			var flame = $("<div/>").attr({class: "flame"}).css(settings.css.flame).appendTo(wrapper);
			var candle = $("<div/>").attr({class: "candle"}).css(settings.css.candle).appendTo(wrapper);
			var stand = $("<div/>").attr({class: "stand"}).css(settings.css.stand).append(settings.stand).appendTo(wrapper);
			
			updateCandle(settings.container);
		}
		
		return this;
	};
	
	function updateCandle(candleContainer) {
		var settings = $.data(candleContainer, "candleSettings");
		
		if (isTimeToShowCandle(settings)) {
			var flameContainer = candleContainer.find(".flame");
			flameContainer.empty();
			flameContainer.append(isTimeToLightCandle(settings) ? settings.flame : settings.wick); 

			var candle = candleContainer.find(".candle");
			candle.empty();
			
			var candleDate = 1;
			var candleTime = 0;
			var candleDayCellCSS = settings.css.candleDayCell;
			var candleDayCellCSSFirst = candleDayCellCSS;
			if (isTimeToLightCandle(settings)) {
				candleDate = (settings.now()).getDate();
				candleTime = (settings.now()).getHours();
				candleDayCellCSSFirst = $.extend(true, {}, candleDayCellCSS, {height: (settings.candleCellHeight -(settings.candleStep * candleTime))+"px"});
			} else {
				$("<div/>").attr({class: "candletop"}).css(settings.css.candletop).append("&nbsp;").appendTo(candle);
			}
			if (candleDate <=24) {
				$("<div/>").attr({class: "candledate"}).css(candleDayCellCSSFirst).append($("<div/>").css(settings.css.candleDay).append(candleDate)).appendTo(candle);
				for(d = candleDate+1 ; d<=24 ; d++) {
					var txt = d;
					$("<div/>").attr({class: "candledate"}).css(candleDayCellCSS).append($("<div/>").css(settings.css.candleDay).append(txt)).appendTo(candle);
				}
			}
			$("<div/>").attr({class: "candlebottom"}).css(settings.css.candlebottom).append("&nbsp;").appendTo(candle);
		}
		var now = settings.now();
		var nextUpdate = settings.now();
		if (isTimeToLightCandle(settings)) {
			nextUpdate.setHours(now.getHours() + 1);
		} else {
			nextUpdate.setDate(now.getDate() + 1);
			nextUpdate.setHours(0);
		}
		nextUpdate.setMinutes(0);
		nextUpdate.setSeconds(0);

		var timeout = (nextUpdate.getTime() - now.getTime())+1000;
		window.setTimeout(function(){ updateCandle(candleContainer); }, timeout);
	}
	
	function isTimeToShowCandle(settings) {
		var now = settings.now();
		return settings.startDate.getTime() <= now.getTime() && now.getTime() <= settings.endDate.getTime(); 
	}
	
	function isTimeToLightCandle(settings) {
		var now = settings.now();
		return (settings.startDate.getTime() <= now.getTime()) && (now.getMonth() == (settings.candleLightMonth - 1));
		
	}
	
}(jQuery));

